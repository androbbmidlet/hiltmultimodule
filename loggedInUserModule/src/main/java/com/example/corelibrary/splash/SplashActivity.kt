package com.example.android.dagger.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android.dagger.main.MainActivity
import com.example.android.dagger.user.UserManager
import com.example.corelibrary.R
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashActivity:AppCompatActivity() {
    @Inject
    lateinit var userManager: UserManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (!userManager.isUserLoggedIn()) {
            if (!userManager.isUserRegistered()) {
                try {
                    val myIntent = Intent(this, Class.forName("com.example.android.dagger.registration.RegistrationActivity"))
                    startActivity(myIntent)
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }

                finish()
            } else {
                try {
                    val myIntent =
                        Intent(this, Class.forName("com.example.android.dagger.login.LoginActivity"))
                    startActivity(myIntent)
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }
                finish()
            }
        } else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}